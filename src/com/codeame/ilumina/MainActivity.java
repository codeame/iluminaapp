package com.codeame.ilumina;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import at.abraxas.amarino.Amarino;

import android.widget.Spinner;
import android.widget.AdapterView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemSelectedListener;
//import android.widget.CheckBox;
import android.widget.Switch;
import android.widget.Toast;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.content.BroadcastReceiver;

import android.content.Intent;
import android.content.IntentFilter;
import at.abraxas.amarino.AmarinoIntent;
import android.content.Context;



public class MainActivity extends Activity implements OnSeekBarChangeListener, OnItemSelectedListener, CompoundButton.OnCheckedChangeListener {

	private static final String TAG = "Ilumina";
	private static final String DEVICE_ADDRESS = "20:13:07:25:24:18";
	private ArduinoReceiver arduinoReceiver = new ArduinoReceiver();
    private ConnectionStateReceiver connectionStateReceiver = new ConnectionStateReceiver();
    
    final int DELAY = 150;
	SeekBar redSB;
	SeekBar greenSB;
	SeekBar blueSB;
    Spinner spinnerMode;
	View colorIndicator;
    //CheckBox chLdr;
	Switch chLdr;
	
	int red, green, blue, mode, useLightSensor;
	long lastChange;

	
	/** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
    	Log.d(TAG, "onCreate() :: Starting.");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

		Amarino.connect(this, DEVICE_ADDRESS); //MCH: Moved to onStart()
        IntentFilter filter = new IntentFilter(AmarinoIntent.ACTION_CONNECTED);
        filter.addAction(AmarinoIntent.ACTION_DISCONNECTED);
        filter.addAction(AmarinoIntent.ACTION_CONNECTION_FAILED);
        // in order to receive broadcasted intents we need to register our receiver
        registerReceiver(arduinoReceiver, new IntentFilter(AmarinoIntent.ACTION_RECEIVED));
        registerReceiver(connectionStateReceiver, filter);
        ///registerReceiver(connectionStateReceiver, new IntentFilter(AmarinoIntent.ACTION_DISCONNECTED));
        //registerReceiver(connectionStateReceiver, new IntentFilter(AmarinoIntent.ACTION_CONNECTION_FAILED));
        
        // get references to views defined in our main.xml layout file
        redSB = (SeekBar) findViewById(R.id.SeekBarRed);
        greenSB = (SeekBar) findViewById(R.id.SeekBarGreen);
        blueSB = (SeekBar) findViewById(R.id.SeekBarBlue);
//        colorIndicator = findViewById(R.id.ColorIndicator);
        spinnerMode = (Spinner) findViewById(R.id.spinner);
        //chLdr = (CheckBox) findViewById(R.id.ldrCheckBox);
        Log.d(TAG, "onCreate() :: Loading chLdr.");
        chLdr = (Switch) findViewById(R.id.switchLdr);


        // register listeners
        redSB.setOnSeekBarChangeListener(this);
        greenSB.setOnSeekBarChangeListener(this);
        blueSB.setOnSeekBarChangeListener(this);
        spinnerMode.setOnItemSelectedListener(this);
        chLdr.setOnCheckedChangeListener(this);

        //disable view
        redSB.setEnabled(false);
        greenSB.setEnabled(false);
        blueSB.setEnabled(false);
        spinnerMode.setEnabled(false);
        chLdr.setEnabled(false);
        
        Log.d(TAG, "onCreate() :: End.");
    }
    
	@Override
	protected void onStart() {
		super.onStart();

		Log.d(TAG, "onStart() :: Starting.");

		// load last state
        Context mContext = getApplicationContext();
        SharedPreferences prefs = mContext.getSharedPreferences("MYPREFS", mContext.MODE_PRIVATE); //PreferenceManager.getSharedPreferences(this);
        red = prefs.getInt("red", 0);
        green = prefs.getInt("green", 0);
        blue = prefs.getInt("blue", 0);
        mode = prefs.getInt("mode", 0);
        useLightSensor = prefs.getInt("useLightSensor", 0);
        Log.d(TAG, "STARTING... RED:"+red);
        Log.d(TAG, "STARTING... GREEN:"+green);
        Log.d(TAG, "STARTING... BLUE:"+blue);
        Log.d(TAG, "STARTING... MODE:"+mode);
        Log.d(TAG, "STARTING... LIGHT SENSOR:"+useLightSensor);

        
        // set seekbars and feedback color according to last state
        spinnerMode.setSelection(mode-1); // Spinner pos count starts at 0, mode starts at 1.
        redSB.setProgress(red);
        greenSB.setProgress(green);
        blueSB.setProgress(blue);
//        colorIndicator.setBackgroundColor(Color.rgb(red, green, blue));

        if (useLightSensor == 1) {
            chLdr.setChecked(true); 
        }
        else {
            chLdr.setChecked(false);
        }

        //disable view
        redSB.setEnabled(false);
        greenSB.setEnabled(false);
        blueSB.setEnabled(false);
        spinnerMode.setEnabled(false);
        chLdr.setEnabled(false);

        //Amarino.connect(this, DEVICE_ADDRESS);

        new Thread(){
        	public void run(){
        		try {
					Thread.sleep(6000);
				} catch (InterruptedException e) {}
				Log.d(TAG, "update colors");
        		//updateAllColors();
        	}
        }.start();
        
	}

	@Override
	protected void onStop() {
		super.onStop();
		// stop Amarino's background service, we don't need it any more 
		Amarino.disconnect(this, DEVICE_ADDRESS);

        // do never forget to unregister a registered receiver
        unregisterReceiver(arduinoReceiver);
        unregisterReceiver(connectionStateReceiver);
	}



	@Override
	public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
		// do not send to many updates, Arduino can't handle so much
		if (System.currentTimeMillis() - lastChange > DELAY ){
			updateState(seekBar);
			lastChange = System.currentTimeMillis();
		}
	}

	@Override
	public void onStartTrackingTouch(SeekBar seekBar) {
		lastChange = System.currentTimeMillis();
	}

	@Override
	public void onStopTrackingTouch(SeekBar seekBar) {
		updateState(seekBar);
	}

	private void updateState(final SeekBar seekBar) {
		
		switch (seekBar.getId()){
			case R.id.SeekBarRed:
				red = seekBar.getProgress();
				updateRed();
				break;
			case R.id.SeekBarGreen:
				green = seekBar.getProgress();
				updateGreen();
				break;
			case R.id.SeekBarBlue:
				blue = seekBar.getProgress();
				updateBlue();
				break;
		}
		// provide user feedback
//		colorIndicator.setBackgroundColor(Color.rgb(red, green, blue));
	}
	
	private void updateAllColors() {
		// send state to Arduino
        updateRed();
        updateGreen();
        updateBlue();
        Log.d(TAG, "updating colors: Colors sent to arduidno.");
	}
	
	private void updateRed(){
		Amarino.sendDataToArduino(this, DEVICE_ADDRESS, 'r', red);
	}
	
	private void updateGreen(){
		Amarino.sendDataToArduino(this, DEVICE_ADDRESS, 'g', green);
	}
	
	private void updateBlue(){
		Amarino.sendDataToArduino(this, DEVICE_ADDRESS, 'b', blue);
	}


    public void onItemSelected(AdapterView<?> parent, View view, int pos, long id)
    {
        int fixedPos = pos+1; // Fixing mode. Valid modes are from 1 to n. Spinner positions are from 0 to n-1.
        Log.d(TAG, "Changing mode. POS:"+fixedPos+" currentMode:"+mode);
        if (fixedPos != mode) {
            //only send if mode is not the same as the new value (has changed before).
            Log.d(TAG, "ITEM SELECTED: "+String.valueOf(spinnerMode.getSelectedItem())+"["+fixedPos+"]" );
            Amarino.sendDataToArduino(this, DEVICE_ADDRESS, 'm', fixedPos);
        } else {Log.d(TAG, "SELECTED ITEM IS THE SAME AS THE SAVED ONE.");}

        //Check if the selected mode is 3 [arcoiris]
        if (fixedPos == 3) {
            //disable the color sliders
            redSB.setEnabled(false);
            greenSB.setEnabled(false);
            blueSB.setEnabled(false);
        } else {
            if (chLdr.isEnabled())
                redSB.setEnabled(true);
            if (chLdr.isEnabled())
                greenSB.setEnabled(true);
            if (chLdr.isEnabled())
                blueSB.setEnabled(true);
        }

    }

    public void onNothingSelected(AdapterView<?> parent)
    {
    	Log.d(TAG, "Nothing selected!");
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
    	Toast.makeText(this, "Sensor de iluminacion "+(isChecked ? "activo":"apagado"), Toast.LENGTH_SHORT).show();
    	Log.d(TAG, "LDR: "+String.valueOf(isChecked));
        if (isChecked)
            Amarino.sendDataToArduino(this, DEVICE_ADDRESS, 'l', 1);
        else
            Amarino.sendDataToArduino(this, DEVICE_ADDRESS, 'l', 0);
    }


    /**
     * ArduinoReceiver is responsible for catching broadcasted Amarino
     * events.
     *
     * It extracts data from the intent and updates the graph accordingly.
     */
    public class ArduinoReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(TAG, "<==> RECEIVING DATA...");
            String data = null;

            // the device address from which the data was sent, we don't need it here but to demonstrate how you retrieve it
            final String address = intent.getStringExtra(AmarinoIntent.EXTRA_DEVICE_ADDRESS);

            // the type of data which is added to the intent
            final int dataType = intent.getIntExtra(AmarinoIntent.EXTRA_DATA_TYPE, -1);

            // we only expect String data though, but it is better to check if really string was sent
            // later Amarino will support differnt data types, so far data comes always as string and
            // you have to parse the data to the type you have sent from Arduino, like it is shown below
            if (dataType == AmarinoIntent.STRING_EXTRA){
                data = intent.getStringExtra(AmarinoIntent.EXTRA_DATA);
                Log.d(TAG, "DATA RECEIVED: "+data);

                if (data != null){
                    try {
                        //getting the different data
                        //Here the mode number is received correctly, arduino sends modes from 1 to n.
                        int num = Integer.parseInt(data.substring(1));
                        if (data.charAt(0) == 'M') {
                            Log.d(TAG, "*SAVING MODE:"+num);
                            context.getSharedPreferences("MYPREFS", Context.MODE_PRIVATE)
                                    .edit()
                                    .putInt("mode", num)
                                    .commit();
                            //Save mode varialbe.
                            mode = num;
                        }// Save MODE
                        else if (data.charAt(0) == 'L') {
                            Log.d(TAG, "*SAVING useLightSensor:"+num);
                            context.getSharedPreferences("MYPREFS", Context.MODE_PRIVATE)
                                    .edit()
                                    .putInt("useLightSensor", num)
                                    .commit();
                            //save variable
                            useLightSensor = num; //it is saved as an int 0/1.
                        }// Save LDR
                        else if (data.charAt(0) == 'R') {
                            Log.d(TAG, "*SAVING RED:"+num);
                            context.getSharedPreferences("MYPREFS", Context.MODE_PRIVATE)
                                    .edit()
                                    .putInt("red", num)
                                    .commit();
                            //save red variable
                            red = num;
                        }// Save RED
                        else if (data.charAt(0) == 'G') {
                            Log.d(TAG, "*SAVING GREEN:"+num);
                            context.getSharedPreferences("MYPREFS", Context.MODE_PRIVATE)
                                    .edit()
                                    .putInt("green", num)
                                    .commit();
                            //save green variable
                            green = num;
                        }// Save GREEN
                        else if (data.charAt(0) == 'B') {
                            Log.d(TAG, "*SAVING BLUE:"+num);
                            context.getSharedPreferences("MYPREFS", Context.MODE_PRIVATE)
                                    .edit()
                                    .putInt("blue", num)
                                    .commit();
                            //saving blue variable
                            blue = num;
                        }// Save MODE
                    }
                    catch (NumberFormatException e) { /* oh data was not an integer */ }
                }
            }
        }
    }


    public class ConnectionStateReceiver extends BroadcastReceiver {
         @Override
         public void onReceive(Context context, Intent intent) {
             final String action = intent.getAction();

             // this is how to retrieve the address of the sender
             final String address = intent.getStringExtra(AmarinoIntent.EXTRA_DEVICE_ADDRESS);

             Log.d(TAG, "RECEIVING CONNECTION STATE:"+action+ " ADDR:"+address);

             if (AmarinoIntent.ACTION_CONNECTED.equals(action)){
                 // connection has been established
                 Log.d(TAG, "Connection established...");
                 //check if Mode is Arcoiris (3)
                 if ( mode == 3) {
                	 Log.d(TAG, "Mode is 3, disabling color sliders");
                	 redSB.setEnabled(false);
                     greenSB.setEnabled(false);
                     blueSB.setEnabled(false);
                     spinnerMode.setEnabled(true);
                	 chLdr.setEnabled(true);
                	 Toast.makeText(context, "Arcoiris activo: Desactivando color sliders", Toast.LENGTH_SHORT).show();
                 } else {
                	 //enable view
                	 redSB.setEnabled(true);
                	 greenSB.setEnabled(true);
                	 blueSB.setEnabled(true);
                	 spinnerMode.setEnabled(true);
                	 chLdr.setEnabled(true);
                }
             }
             else if (AmarinoIntent.ACTION_DISCONNECTED.equals(action)){
                 // disconnected from a device
                 Log.d(TAG, "Connection lost...");
                 //disable view
                 redSB.setEnabled(false);
                 greenSB.setEnabled(false);
                 blueSB.setEnabled(false);
                 spinnerMode.setEnabled(false);
                 chLdr.setEnabled(false);
                 }
             else if (AmarinoIntent.ACTION_CONNECTION_FAILED.equals(action)){
                 // connection attempt was not successful
                 Log.d(TAG, "Connection failed...");
                 redSB.setEnabled(false);
                 greenSB.setEnabled(false);
                 blueSB.setEnabled(false);
                 spinnerMode.setEnabled(false);
                 chLdr.setEnabled(false);
                 }
             else if (AmarinoIntent.ACTION_PAIRING_REQUESTED.equals(action)){
                 // a notification message to pair the device has popped up
                 Log.d(TAG, "Connection Pairing request...");
                 }
             }
         }
	
	
	
	
	
	
	
	
	
	
	


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
